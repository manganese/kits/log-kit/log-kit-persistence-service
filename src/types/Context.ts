import { Logger } from 'simple-node-logger';
import { Sequelize } from 'sequelize';
import { Queue } from '@manganese/log-kit-core';
import { OwntracksLocationEvent, RadiationEvent } from '../models';
import {
  OwntracksPersistenceService,
  RadiationPersistenceService,
} from '../services';
import { Configuration } from '.';

export interface Context {
  configuration: Configuration;
  log: Logger;
  queue: Queue;
  sequelize: Sequelize;
  models: {
    OwntracksLocationEvent: typeof OwntracksLocationEvent;
    RadiationEvent: typeof RadiationEvent;
  };
  services: {
    owntracksPersistenceService: OwntracksPersistenceService;
    radiationPersistenceService: RadiationPersistenceService;
  };
}
