export interface RadiationPersistenceConfiguration {
  sensorIds?: Record<string /* GID */, string /* sensor ID */>;
}
