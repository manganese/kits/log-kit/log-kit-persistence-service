import { Dialect } from 'sequelize';

export interface SequelizeConfiguration {
  host: string;
  port?: number;
  database: string;
  username: string;
  password: string;
  dialect: Dialect;
}
