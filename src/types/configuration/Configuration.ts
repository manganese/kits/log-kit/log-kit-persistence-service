import { QueueConfiguration } from '@manganese/log-kit-core';
import {
  OwntracksPersistenceConfiguration,
  RadiationPersistenceConfiguration,
  SequelizeConfiguration,
} from '.';

export interface Configuration {
  queue: QueueConfiguration;
  sequelize: SequelizeConfiguration;
  persistence: {
    owntracks: OwntracksPersistenceConfiguration;
    radiation: RadiationPersistenceConfiguration;
  };
}
