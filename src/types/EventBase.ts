export interface EventBase {
  id: string;
  timestamp: Date;
}
