#!/usr/bin/env node
import yargs from 'yargs';
import { createSimpleLogger } from 'simple-node-logger';
import * as pg from 'pg';
import { Sequelize } from 'sequelize';
import { createQueue } from '@manganese/queue-kit';
import {
  Message,
  MessageType,
  OwntracksMessage,
  RadiationMessage,
} from '@manganese/log-kit-core';
import { Context } from '../types';
import {
  OwntracksLocationEvent,
  RadiationEvent,
  initializeModels,
  OwntracksPersistenceService,
  RadiationPersistenceService,
  parseConfigurationJson,
  validateConfiguration,
} from '..';

yargs
  .scriptName('log-kit-persistence-service')
  .usage('$0 <command> [arguments]')
  .command<{
    configurationJson: string;
  }>(
    'start',
    'Start a LogKit persistence service',
    (yargs) => {
      yargs.option('configuration-json', {
        type: 'string',
        required: true,
        describe: 'Configuration JSON',
      });
    },
    async (argv) => {
      const configuration = parseConfigurationJson(argv.configurationJson);
      const log = createSimpleLogger();

      try {
        validateConfiguration(configuration);

        const sequelize = new Sequelize(
          configuration.sequelize.database,
          configuration.sequelize.username,
          configuration.sequelize.password,
          {
            host: configuration.sequelize.host,
            port: configuration.sequelize.port,
            dialect: 'postgres',
            dialectModule: pg,
          }
        );

        await sequelize.authenticate();

        log.info('Database connection established successfully');

        initializeModels(sequelize);

        const queue = createQueue<Message>(configuration.queue);

        await queue.initialize();

        log.info('Queue initialized successfully');

        const context: Context = {
          configuration,
          log,
          queue,
          sequelize,
          models: {
            OwntracksLocationEvent,
            RadiationEvent,
          },
          services: {
            owntracksPersistenceService: null,
            radiationPersistenceService: null,
          },
        };
        context.services.owntracksPersistenceService =
          new OwntracksPersistenceService(context);
        context.services.radiationPersistenceService =
          new RadiationPersistenceService(context);

        await queue.subscribe(async (message) => {
          log.debug('Queue subscription message:', message);

          switch (message.type) {
            case MessageType.Owntracks:
              return context.services.owntracksPersistenceService.handleMessage(
                message as OwntracksMessage
              );
            case MessageType.Radiation:
              return context.services.radiationPersistenceService.handleMessage(
                message as RadiationMessage
              );
            default:
              throw new Error(`Unknown message type: ${message}`);
          }
        });
      } catch (error) {
        log.fatal(error);
      }
    }
  )
  .help().argv;
