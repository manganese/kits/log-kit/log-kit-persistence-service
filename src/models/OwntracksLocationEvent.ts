import { Sequelize, DataTypes, Model } from 'sequelize';
import { OwntracksLocationPayload } from '@manganese/log-kit-core';
import { EventBase } from '../types';
import { EVENT_BASE_ATTRIBUTES } from '../constants';

export class OwntracksLocationEvent extends Model implements EventBase {
  public id: string;
  public timestamp: Date;
  public userId: string;
  public deviceId: string;
  public payload: OwntracksLocationPayload;
}

export const initializeOwntracksLocationEvent = (sequelize: Sequelize) => {
  OwntracksLocationEvent.init(
    {
      ...EVENT_BASE_ATTRIBUTES,
      userId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      deviceId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      payload: {
        type: DataTypes.JSONB,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'OwntracksLocationEvent',
      timestamps: false,
    }
  );
};
