import { Sequelize } from 'sequelize';

import { initializeOwntracksLocationEvent } from './OwntracksLocationEvent';
import { initializeRadiationEvent } from './RadiationEvent';

export * from './OwntracksLocationEvent';
export * from './RadiationEvent';

export const initializeModels = (sequelize: Sequelize) => {
  initializeOwntracksLocationEvent(sequelize);
  initializeRadiationEvent(sequelize);
};
