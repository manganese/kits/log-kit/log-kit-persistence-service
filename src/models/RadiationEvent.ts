import { Sequelize, DataTypes, Model } from 'sequelize';
import { EventBase } from '../types';
import { EVENT_BASE_ATTRIBUTES } from '../constants';

export class RadiationEvent extends Model implements EventBase {
  public id: string;
  public timestamp: Date;
  public sensorId: string;
  public cpm: number;
}

export const initializeRadiationEvent = (sequelize: Sequelize) => {
  RadiationEvent.init(
    {
      ...EVENT_BASE_ATTRIBUTES,
      sensorId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      cpm: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'RadiationEvent',
      timestamps: false,
    }
  );
};
