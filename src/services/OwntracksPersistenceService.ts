import moment, { Moment } from 'moment';
import {
  OwntracksLocationPayload,
  OwntracksMessage,
  OwntracksPayload,
  OwntracksPayloadType,
  getOwntracksLocationPayloadIds,
  getOwntracksLocationPayloadTimestamp,
} from '@manganese/log-kit-core';
import { Context } from '../types';
import { OwntracksLocationEvent } from '../models';

export class OwntracksPersistenceService {
  constructor(private readonly context: Context) {}

  public async handleMessage(message: OwntracksMessage) {
    switch (message.payload._type) {
      case OwntracksPayloadType.Location:
        await this.handleLocationMessage(message);

        break;
      default:
        this.context.log.info(
          `Ignored OwnTracks ${message.payload._type} payload`
        );

        break;
    }
  }

  private async handleLocationMessage(message: OwntracksMessage) {
    const payload = message.payload as OwntracksLocationPayload;
    const timestamp = getOwntracksLocationPayloadTimestamp(payload);
    const { userId, deviceId } = getOwntracksLocationPayloadIds(payload);
    const event = await OwntracksLocationEvent.create({
      timestamp: timestamp.toDate(),
      userId,
      deviceId,
      payload,
    });

    this.context.log.info(
      `Handled OwnTracks location payload for ${userId}:${deviceId} -> ${event.id}`
    );
  }
}
