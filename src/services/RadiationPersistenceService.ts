import { RadiationMessage, RadiationPayload } from '@manganese/log-kit-core';
import { Context } from '../types';
import { RadiationEvent } from '../models';

export class RadiationPersistenceService {
  constructor(private readonly context: Context) {}

  public async handleMessage(message: RadiationMessage) {
    const { payload } = message;
    const { AID, GID, CPM: cpm } = payload;

    if (AID !== '1') {
      this.context.log.info(
        `Ignored radiation payload due to unrecognized AID`
      );

      return;
    }

    let sensorId = GID;
    const sensorIds =
      this.context.configuration.persistence?.radiation?.sensorIds;

    if (sensorIds) {
      if (sensorIds[GID]) {
        sensorId = sensorIds[GID];

        this.context.log.debug(
          `Mapping radiation payload GID ${GID} -> sensor ID ${sensorId}`
        );
      } else {
        this.context.log.info(
          `Ignored radiation payload due to unrecognized GID`
        );

        return;
      }
    }

    const event = await RadiationEvent.create({
      sensorId,
      cpm,
    });

    this.context.log.info(
      `Handled radiation payload for ${sensorId} of ${cpm}CPM -> ${event.id}`
    );
  }
}
