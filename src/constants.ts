import { DataTypes, ModelAttributes } from 'sequelize';

export const EVENT_BASE_ATTRIBUTES: ModelAttributes = {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    allowNull: false,
    primaryKey: true,
  },
  timestamp: {
    type: DataTypes.DATE,
    defaultValue: DataTypes.NOW,
    allowNull: false,
  },
};

// export const ID_BY_FINDMY_DEVICE_UUIDS = new Map([
//   [
//     '0sV15t4s4IJCuoG7S6bZNv04lVf6RpaIQOG1F1fceJpZHuo33fhrvp00jtZuU34e4PIXkn9U9athIEvKH6FSHuHYVNSUzmWV',
//     'sam-airpods-max',
//   ],
//   ['LRQXI2hDUpGzlt+PElvhYXws0IMxxVb/i7mxiFvihSk=', 'sam-watch'],
//   ['sDyVSTJQqe5R6pGyI/vYFIFqMAnSM4nOig465MVN4B1YFKyp31DuKQ==', 'sam-laptop'],
//   ['5yAbrrC4uERIFjBfpK6q6TUxQjXBOf7mi7mxiFvihSk=', 'sam-phone'],
//   ['n6ofM9CX4jApf+gqW4HZrgTuzjblqOP+i7mxiFvihSk=', 'sam-tablet'],
//   ['2g30WYRCZMgFJq5BWFG+Fo6fwpVkKQ9eBXYYJcsiJdlie3IHso+AWg==', 'dottid-laptop'],
//   [
//     'A0/uGJUMOkwwq0wlLv5FzKb02uaCGJY+zYN8jakTOwaPnJ5PBZsyh8+Q8MmFCeiHBOrz65s8CfpaNO2p4gMMquHYVNSUzmWV',
//     'sam-airpods-pro',
//   ],
//   [
//     'shHKbFhVJLcVsf9jVchi5Kea7pnXXtVuyiD+SxFexxJd2inQqbJ5VLdql/vZjcKyijfICmyNu4i7UQu9yK8ln+HYVNSUzmWV',
//     'sam-airpods',
//   ],
// ]);

// export const ID_BY_FINDMY_ITEM_UUIDS = new Map([
//   ['05216DA9-4427-42DC-9321-6514EC927A6F', 'suitcase-airtag'],
//   ['C4E8D236-2328-4B06-8A1A-BEDC1BACEF2A', 'jacket-airtag'],
//   ['D0603231-4521-49D0-8025-400D01F2910C', 'stethoscope-airtag'],
//   ['86636FE9-282C-4AFD-B85B-87B74625E0D3', 'wallet'],
//   ['F310525C-56E4-471F-A58B-6469FD8BE9C9', 'primary-keys-airtag'],
//   ['B70BFF3F-734A-41A9-9681-755730A486C7', 'backpack-airtag'],
//   ['A52F381F-F95A-43E3-88AA-CC6EDF69F558', 'cat-carrier-airtag'],
//   ['A4FA3B94-73CC-41E8-9366-A10079CC3FFA', 'messenger-bag-airtag'],
//   ['8F7A3EAB-4424-4A11-A785-6FCCCFE9E0C7', 'spare-keys-airtag'],
//   ['BCAC9519-7823-4E34-A88D-C443E8E0EFEC', 'car-keys-airtag'],
//   ['AB897CCC-6B47-42E1-B967-ECD625B76A37', 'car-airtag'],
//   ['D8A55755-4EED-4925-8BEA-31620F596AA1', 'truck-airtag'],
//   ['CF0B0922-A6FE-40DB-B463-E3DBEDE81F0B', 'bike-airtag'],
//   ['4A3D60A2-B879-4295-B3FA-EB6A89E5089A', 'sam-airpods-max'],
//   ['146536E4-54EA-42A9-8FA5-A5DF7A346CF7', 'sam-airpods-pro-right-bud'],
//   ['D6BB9DCD-F0B8-4131-BE99-B8D646EAE2D6', 'sam-airpods-pro-left-bud'],
//   ['28770255-E913-4A2B-A4C6-E70B1FB9FE6E', 'sam-airpods-pro-case'],
// ]);
