import { Configuration } from '../../types';

export const parseConfigurationJson = (
  configurationJson: string
): Configuration => {
  return JSON.parse(configurationJson);
};
